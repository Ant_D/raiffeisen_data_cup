import numpy as np
from collections import defaultdict
from utils import distance, INF


AMOUNT = "amount"
ATM_LAT = "atm_address_lat"
ATM_LON = "atm_address_lon"
CUSTOMER_ID = "customer_id"
HOME_LAT = "home_add_lat"
HOME_LON = "home_add_lon"
POS_LAT = "pos_address_lat"
POS_LON = "pos_address_lon"
WORK_LAT = "work_add_lat"
WORK_LON = "work_add_lon"


def get_headers_idx(file_name):
    top = np.genfromtxt(file_name,
                        encoding="utf-8",
                        dtype=str,
                        delimiter=",",
                        max_rows=1)
    headers = dict()
    for c, h in enumerate(top):
        headers[h] = c
    return headers


def read_transactions(file_name, istest=False, max_rows=None):
    headers_idx = get_headers_idx(file_name)

    used_cols = (ATM_LAT, ATM_LON, CUSTOMER_ID, HOME_LAT, HOME_LON, POS_LAT, POS_LON, WORK_LAT, WORK_LON)
    if istest:
        used_cols = (ATM_LAT, ATM_LON, CUSTOMER_ID, POS_LAT, POS_LON)

    def row_to_dict(row):
        d = defaultdict(lambda: np.nan)
        for h in used_cols:
            if headers_idx.get(h) is not None:
                d[h] = row[headers_idx[h]]
        return d

    float_cols = (ATM_LAT, ATM_LON, HOME_LAT, HOME_LON, POS_LAT, POS_LON, WORK_LAT, WORK_LON)
    if istest:
        float_cols = (ATM_LAT, ATM_LON, POS_LAT, POS_LON)

    float_cols_idx = (headers_idx[h] for h in float_cols)

    data_set = map(row_to_dict,
                   np.genfromtxt(file_name,
                                 encoding="utf-8",
                                 dtype=None,
                                 delimiter=",",
                                 comments="\0",
                                 skip_header=1,
                                 max_rows=max_rows,
                                 converters=dict().fromkeys(float_cols_idx, lambda x: float(x) if x else np.nan)
                                 ))

    transactions = np.array(list(filter(lambda x: x.isvalid(),
                                        map(TransactionRecord, data_set))), dtype=object)

    return transactions


def write_answers(file_name, answers):
    with open(file_name, "w") as f:
        print(CUSTOMER_ID, WORK_LAT, WORK_LON, HOME_LAT, HOME_LON, file=f, sep=',')
        for customer, ans in answers.items():
            print(customer, ans.work[0], ans.work[1], ans.home[0], ans.home[1], file=f, sep=',')


class TransactionRecord:
    def __init__(self, data):
        self.customer_id = data[CUSTOMER_ID]
        self.amount = data[AMOUNT]
        self.home = np.array((data[HOME_LAT], data[HOME_LON]))
        self.work = np.array((data[WORK_LAT], data[WORK_LON]))
        self.pos = np.array((data[ATM_LAT], data[ATM_LON]))
        if np.isnan(self.pos).any():
            self.pos = np.array((data[POS_LAT], data[POS_LON]))
        self.cnt = 1

    def has_work(self):
        return not np.isnan(self.work).any()

    def has_home(self):
        return not np.isnan(self.home).any()

    def weight(self, get_y):
        return min(self.cnt / distance(self.pos, get_y(self)), INF)

    def isvalid(self):
        return not np.isnan(self.pos).any()

    def __repr__(self):
        return 'TR{{id: {}, home: {}, work: {}, pos: {}}}' \
            .format(self.customer_id, self.home, self.work, self.pos)

    def __hash__(self):
        return tuple([self.customer_id, tuple(self.pos)]).__hash__()

    def __eq__(self, other):
        return isinstance(other, TransactionRecord) and \
               self.customer_id == other.customer_id and \
               (self.pos == other.pos).all() and \
               (self.home == other.home).all() and \
               (self.work == other.work).all()