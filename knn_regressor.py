import numpy as np
from sklearn.neighbors import KDTree
from utils import *


class KNNRegressor:
    def __init__(self, k_neighbors):
        self.k_neighbors = k_neighbors
        self.tree = None
        self.train_x = None
        self.train_y = None
        self.train_w = None

    def train(self, x, y, w):
        self.tree = KDTree(x)
        self.train_x = x
        self.train_y = y
        self.train_w = w

    def predict(self, test_x, test_w):
        test_y = []
        for i in range(len(test_x)):
            y0 = self.predict_for_one(test_x[i])
            test_y.append(y0)
        return np.average(test_y, axis=0, weights=test_w)

    def predict_for_one(self, x):
        dist, neigh = self.tree.query([x, ], k=min(self.k_neighbors, len(self.train_x)))
        dist = dist[0]
        neigh = neigh[0]
        weights = [min(INF, self.train_w[neigh[i]] / dist[i]) for i in range(len(neigh))]
        y = list(map(lambda n: self.train_y[n], neigh))
        y0 = np.average(y, axis=0, weights=weights)
        return y0