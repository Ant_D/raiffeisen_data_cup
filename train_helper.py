from collections import Counter
from utils import *


class TrainHelper:
    def __init__(self, dist_treshold=1, cnt_treshold=1):
        self.dist_treshold = dist_treshold
        self.cnt_treshold = cnt_treshold

    def dist_ok(self, t):
        if t.has_work():
            return min(distance(t.home, t.pos), distance(t.work, t.pos)) <= self.dist_treshold
        else:
            return distance(t.home, t.pos) <= self.dist_treshold

    def cnt_ok(self, t):
        return t.cnt > self.cnt_treshold

    def extract_best(self, transactions):
        return list(filter(lambda t: self.dist_ok(t) and self.cnt_ok(t), transactions))

    def extract_home_trans(self, transactions):
        home_trans = []
        for t in transactions:
            if has_nan(t.work) or distance(t.pos, t.home) <= distance(t.pos, t.work):
                home_trans.append(t)
        return home_trans

    def extract_work_trans(self, transactions):
        work_trans = []
        for t in transactions:
            if not has_nan(t.work) and distance(t.pos, t.work) <= distance(t.pos, t.home):
                work_trans.append(t)
        return work_trans

    def extract_x_y(self, transactions, get_y):
        pos_to_y = defaultdict(lambda: [np.zeros(2), 0])  # first is sum of weighted y, second is sum of weights
        for t in transactions:
            y = pos_to_y[tuple(t.pos)]
            y[0] += t.weight(get_y) * get_y(t)
            y[1] += t.weight(get_y)
        x = list(pos_to_y.keys())
        y = list(map(lambda z: z[0] / z[1], pos_to_y.values()))
        w = list(map(lambda z: z[1], pos_to_y.values()))
        return x, y, w

    def prepare(self, transactions):
        transactions = count(transactions)
        transactions = self.extract_best(transactions)
        home_trans = self.extract_home_trans(transactions)
        work_trans = self.extract_work_trans(transactions)
        home_x, home_y, home_w = self.extract_x_y(home_trans, lambda t: t.home)  # transaction positions and home coordinates they specify
        work_x, work_y, work_w = self.extract_x_y(work_trans, lambda t: t.work)  # transaction positions and work coordinates they specify
        return home_x, home_y, home_w, work_x, work_y, work_w
