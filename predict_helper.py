from utils import *


class PredictHelper():
    def __init__(self):
        pass

    def prepare(self, transactions):
        transactions = count(transactions)
        x = list(map(lambda t: t.pos, transactions))
        w = list(map(lambda t: t.cnt, transactions))
        mid = np.average(x, axis=0, weights=w)
        for i in range(len(w)):
            w[i] = min(w[i] / distance(x, mid), INF)
        return x, w, x, w