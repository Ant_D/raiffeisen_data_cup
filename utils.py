import numpy as np
from collections import defaultdict, namedtuple, Counter
from itertools import chain


INF = 1e12


Answer = namedtuple("Answer", ["home", "work"])


def group_by_customer(transactions):
    g = defaultdict(list)
    for idx, t in enumerate(transactions):
        g[t.customer_id].append(t)
    return list(map(list, (g.items())))


def count(transactions):
    counter = Counter(transactions)

    def unite(p):
        p[0].cnt = p[1]
        return p[0]

    return list(map(unite, counter.items()))


def join(cust_trans):
    return np.array([*chain(*map(lambda x: x[1], cust_trans))])


def has_nan(p):
    return np.isnan(p).any()


def distance(p1, p2):
    return np.sqrt(np.sum((p1 - p2) * (p1 - p2)))