// Remove commas inside string

#include <cstdio>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

void filter(string &line) {
    bool insideString = false;
    for (char &c: line) {
        if (insideString) {
            if (c == ',') {
                c = ' ';
            }
            else if (c == '"') {
                insideString = false;
            }
        }
        else {
            if (c == '"') {
                insideString = true;
            }
        }
    }
}

int main() {
//#ifdef _DEBUG
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
//#endif

    string line;

    while (getline(cin, line)) {
        filter(line);
        cout << line << "\n";
    }

    return 0;
}