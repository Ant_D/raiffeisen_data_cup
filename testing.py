import numpy as np
from utils import *
from data_process import read_transactions
from solver import Solver

TRAIN_FILE = "filtered_train_set.csv"


def test_solver(solver, train_X, test_X, bias=0.02, show_bad_answers=False):
    solver.train(transactions=train_X)

    answers = solver.predict(join(test_X))

    true_answers = dict()
    for customer, trans in test_X:
        true_answers[customer] = Answer(home=trans[0].home, work=trans[0].work)

    bad_answers = []
    correct_cnt = 0
    for customer, ans in answers.items():
        if distance(ans.home, true_answers[customer].home) <= bias or \
                distance(ans.work, true_answers[customer].work) <= bias:
            correct_cnt += 1
        elif show_bad_answers:
            bad_answers.append((customer, ans))
    accuracy = correct_cnt / len(answers)

    if show_bad_answers:
        return accuracy, bad_answers
    else:
        return accuracy


def cross_validation(solver, cust_trans, permutation, test_part=0.25, bias=0.02):
    ntests = int(test_part * len(cust_trans))
    istest = np.zeros(len(permutation), dtype=bool)
    accuracy = 0
    for i in range((len(istest) + ntests - 1) // ntests):
        start = i * ntests
        finish = min((i + 1) * ntests, len(istest))
        istest[start:finish] = True
        test_X = cust_trans[permutation[istest]]
        train_X = join(cust_trans[permutation[~istest]])
        accuracy += (test_solver(solver, train_X, test_X, bias=bias) - accuracy) / (i + 1)
        istest[start:finish] = False
    return accuracy


def testing():
    transactions = read_transactions(TRAIN_FILE)
    cust_trans = np.array(group_by_customer(transactions))
    permutation = np.random.permutation(range(len(cust_trans)))
    for k in range(8, 9):
        print('k ==', k, flush=True)
        solver = Solver(k_neighbors=k, cnt_treshold=0)
        print(cross_validation(solver, cust_trans, permutation))


def find_bad_answers(solver, test_part=0.25, bias=0.02):
    transactions = read_transactions(TRAIN_FILE)
    cust_trans = np.array(group_by_customer(transactions))
    ntests = int(test_part * len(cust_trans))
    test_X = cust_trans[:ntests]
    train_X = join(cust_trans[ntests:])
    accuracy, bad_answers = test_solver(solver, train_X, test_X, bias=bias, show_bad_answers=True)
    return accuracy

if __name__ == "__main__":
    # testing()
    solver = Solver(k_neighbors=8, cnt_treshold=0)
    acc1 = find_bad_answers(solver, bias=0.02)
    acc2 = find_bad_answers(solver, bias=0.04)
    print(acc2 - acc1)