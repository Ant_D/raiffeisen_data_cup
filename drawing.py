from matplotlib.pyplot import scatter
from collections import Counter


def draw_points(p, labels, weights):
    label_to_color = ['r', 'b', 'g', 'y']
    colors = [label_to_color[l] for l in labels]
    x = list(map(lambda z: z[0], p))
    y = list(map(lambda z: z[1], p))
    scatter(x=x, y=y, c=colors, s=weights)


def draw_customer_info(transactions):
    pos = list(map(lambda x: tuple(x.pos), transactions))
    home = tuple(transactions[0].home)
    work = tuple(transactions[0].work)

    #     kmeans = KMeans(n_clusters=2).fit(pos)
    #     labels = sorted(list(dict(zip(pos, kmeans.labels_)).items()))
    #     labels = list(map(lambda x: x[1], labels))

    cnt = sorted(list(dict(Counter(pos)).items()))
    pos = sorted(list(map(lambda x: x[0], cnt)))
    cnt = list(map(lambda x: x[1], cnt))

    labels = [2 for p in pos]

    draw_points([home, work] + pos, [0, 1] + labels, [10, 10] + cnt)