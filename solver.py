from train_helper import TrainHelper
from predict_helper import PredictHelper
from knn_regressor import KNNRegressor
from utils import *


class Solver:
    def __init__(self, k_neighbors, dist_treshold=1, cnt_treshold=1):
        self.home_neigh = KNNRegressor(k_neighbors)
        self.work_neigh = KNNRegressor(k_neighbors)
        self.train_helper = TrainHelper(dist_treshold, cnt_treshold)
        self.predict_helper = PredictHelper()

    def train(self, transactions):
        home_x, home_y, home_w, work_x, work_y, work_w = self.train_helper.prepare(transactions)
        self.home_neigh.train(home_x, home_y, home_w)
        self.work_neigh.train(work_x, work_y, work_w)

    def predict(self, transactions):
        test_set = group_by_customer(transactions)
        answers = dict()
        for cust, trans in test_set:
            answers[cust] = self.predict_for_one_customer(trans)
        return answers

    def predict_for_one_customer(self, transactions):
        home_x, home_w, work_x, work_w = self.predict_helper.prepare(transactions)
        home = self.home_neigh.predict(home_x, home_w)
        work = self.work_neigh.predict(work_x, work_w)
        if has_nan(home): home = work
        if has_nan(work): work = home
        return Answer(home=home, work=work)